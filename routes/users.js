var express = require('express')
var router = express.Router()
var userController = require('../controller/UsersController')

/* GET users listing. */
router.get('/', function (req, res, next) {
  res.json(userController.getUsers())
})
router.get('/:id', function (req, res, next) {
  const { id } = req.params
  res.json(userController.getUser(id))
})
router.post('/', function (req, res, next) {
  const payload = req.body
  res.json(userController.addUser(payload))
})
router.put('/', function (req, res, next) {
  const payload = req.body
  res.json(userController.updateUser(payload))
})
router.delete('/:id', function (req, res, next) {
  const { id } = req.params
  res.json(userController.deleteUser(id))
})
module.exports = router
